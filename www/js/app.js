(function(){
  'use strict';
  var module = angular.module('app', ['onsen']);

  module.controller('TabsController', function() {
  setImmediate(function() {
    var tabIndex = navi.getCurrentPage().options.params.tab;
    app.tabbar.setActiveTab(tabIndex);
  });
});

module.controller('DropdownController', function($scope) {

  ons.ready(function() {
    ons.createPopover('popover.html').then(function(popover) {
      $scope.popover = popover;
    });
  });
  
  $scope.options = ['Load', 'Sync', 'Settings'];
});
  module.controller('AppController', function($scope, $data) {
    $scope.doSomething = function() {
      ons.notification.alert({message: 'Hello, World!'});
    };

    // document.addEventListener("backbutton", function (  ) {

    // } );
  });

  module.controller('DetailController', function($scope, $data) {
    $scope.item = $data.selectedItem;
  });

  module.controller('ViewCampaignController', function($scope, $data) {

  });

  module.controller('ProductListController', function($scope) {
    ons.ready(function() {
    ons.createPopover('popover.html').then(function(popover) {
      $scope.popover = popover;
    });
  });
  
  $scope.options = ['Load', 'Sync', 'Settings'];
  });

  module.controller('ProductDetailsController', function($scope, $data) {

  });

  module.controller('PitchController', function($scope, $data) {

  });

  module.controller('ProfileController', function($scope, $data) {

  });
  module.controller('CartController', function($scope, $data) {

  });

  module.controller('SpreeCommerceController', function($scope, $data) {

  });

  module.controller('MyCartController', function($scope, $data) {

  });

  

  module.controller('EmptyController', function($scope, $data) {

  });



  module.controller('MasterController', function($scope, $data) {
    $scope.items = $data.items;  
    
    $scope.showDetail = function() {
      // var selectedItem = $data.items[index];
      // $data.selectedItem = selectedItem;
      $scope.navi.pushPage('home.html');
    };


    
  });

  module.factory('$data', function() {
      var data = {};
      
      data.items = [
          { 
              title: 'Item 1 Title',
              label: '4h',
              desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
          },
          { 
              title: 'Another Item Title',
              label: '6h',
              desc: 'Ut enim ad minim veniam.'
          },
          { 
              title: 'Yet Another Item Title',
              label: '1day ago',
              desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
          },
          { 
              title: 'Yet Another Item Title',
              label: '1day ago',
              desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
          }
      ]; 
      
      return data;
  });
})();

